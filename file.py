from random import randint, choice
import time
import os
import shutil

"""
This module is used to generate ****.json files one by one every second. 
"""


def main():
    folder = 'data/test'

    # clearing the folder
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))

    # generating files
    for i in range(1000):
        if choice([0, 1]) == 0:
            content_json = r'{"user": "' + choice(['A', 'B', 'C', 'D']) + '", "timestamp":"2017-12-19 10:' \
                           + str(randint(0, 59)) + ':49", "spend":"' + str(randint(0, 100)) + '.99","evtname":"'\
                           + choice(['iap', 'tutorial']) + '"}'
        else:
            content_json = r'{"user": "' + choice(['A', 'B', 'C', 'D']) + '", "timestamp":"2017-12-19 10:' \
                           + str(randint(0, 59)) + ':49", "evtname":"' + choice(['iap', 'tutorial']) + '"}'
        filename = str(i).zfill(4)
        with open(folder +
                  '/{}.json'.format(filename), 'w') as writefile:
            writefile.write(content_json)
        print('creating file {}.json'.format(filename))
        time.sleep(1)


if __name__ == '__main__':
    main()
