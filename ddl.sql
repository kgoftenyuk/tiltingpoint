CREATE TABLE public.users (
	"user" varchar NULL,
	"timestamp" varchar NULL,
	spend varchar NULL,
	evtname varchar NULL
);


CREATE OR replace VIEW revenue
AS
  SELECT "user",
         SUM(spend :: FLOAT) AS total_spend
  FROM   public.users
  GROUP  BY "user"; 
  
 
 
--select * from users;
--select * from revenue;