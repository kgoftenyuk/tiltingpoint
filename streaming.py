from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.streaming import StreamingContext


"""
This is a streaming app. Which redirects data from .json files to postgres table. 
"""


# here we are transforming rdd to df. Then df is loaded into postgres.
def process_stream(record, spark):
    if not record.isEmpty():
        df = spark.read.json(record)
        df.show()
        df.write.mode('append') \
            .jdbc("jdbc:postgresql://localhost:5432/postgres",
                  "users",
                  properties={"user": "postgres", "password": "9239"})


# creating a textFileStream
def main():
    sc = SparkContext(appName="PysparkStreaming")
    spark = SparkSession(sc)
    ssc = StreamingContext(sc, 3)   #every 3 seconds
    lines = ssc.textFileStream('C:/Users/game/PycharmProjects/pythonProject/spark_streaming_test/spark-streaming-master'
                               '/data/test/')
    lines.foreachRDD(lambda rdd: process_stream(rdd, spark))
    ssc.start()
    ssc.awaitTermination()


if __name__ == "__main__":
    main()
