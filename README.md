===============
KGoftenyuk probation
===============

Task
========
5.	Assume we already have real time streaming data coming to our AWS S3, write some code to process the real time streaming data and have it REAL-TIME available in tables? (You can use Spark, Scala, Java, Python, etc. whatever the coding language you want)
Assume the input data source folder is: “s3://data/test”. Inside the “test” folder, it will have thousands of json files like: 0000.json, 0001.json, 0002.json …
To simplify, example records JSON will look as follows:

{“user”: “A”, “timestamp”:”2017-12-19 10:41:49”, “spend”:”1.99”,”evtname”:”iap”}
{“user”: “B”, “timestamp”:”2017-12-20 11:22:18”, ”evtname”:”tutorial”}
{“user”: “A”, “timestamp”:”2017-12-20 18:20:10”, “spend”:”9.99”,”evtname”:”iap”}

Output: two tables (all data types are string)
table user
column: user, timestamp, evtname
table revenue
column: user, total_spend


Requirements
============

- Configure local instance of Apache Spark
- Configure postgresql 12 with database named postgres
- install pyspark, random


Execution
=============

- run ddl.sql in sql console
- run file.py
- run spark app locally


